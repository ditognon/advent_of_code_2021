import numpy as np

def load_data(filename):
    file = open(filename)
    pairs = []
    fold_commands = []
    state = False
    for row in file:
        if(row == '\n'):
            state = True
            continue
        else:
            if not state:
                row = row[:-1]
                tmp_split = row.split(',')
                pairs.append((int(tmp_split[0]), int(tmp_split[1])))
            else:
                row_split = row.split()
                fold_commands.append(row_split[2])
                

    maxx = -1
    maxy = -1
    for pair in pairs:
        if pair[0] > maxx:
            maxx = pair[0]
        if pair[1] > maxy:
            maxy = pair[1]
    paper = np.zeros((895, maxx + 1))

    # Mark pairs on paper
    for pair in pairs:
        paper[pair[1]][pair[0]] = 1
    px = maxx + 1
    py = maxy + 1
    return paper, fold_commands, px, 895

def fold_x(paper, line, px, py):
    # Mark original
    new_px = line
    new_paper = np.zeros((py, line))
    for i in range(py):
        for j in range(line):
            new_paper[i][j] = paper[i][j]

    # Mark folded
    for i in range(py):
        for j in range(px - line):
            if paper[i][px - j - 1] == 1:
                new_paper[i][j] = 1

    return new_paper, new_px, py

def fold_y(paper, line, px, py):
    new_py = line
    new_paper = np.zeros((line, px))
    # Mark original
    for i in range(line):
        for j in range(px):
            new_paper[i][j] = paper[i][j]

    # Mark folded
    for i in range(py - line - 1):
        for j in range(px):
            if paper[py - i - 1][j] == 1:
                new_paper[i][j] = 1

    return new_paper, px, new_py

def fold(paper, fold_command, px, py):
    tmp_cmd = fold_command.split('=')
    if(tmp_cmd[0] == 'x'):
        new_paper, px, py = fold_x(paper, int(tmp_cmd[1]), px, py)
    else:
        new_paper, px, py = fold_y(paper, int(tmp_cmd[1]), px, py)
    
    return new_paper, px, py

def count_dots(paper, x, y):
    cnt = 0
    for i in range(y):
        for j in range(x):
            if paper[i][j] == 1:
                cnt += 1

    return cnt


paper, fold_commands, px, py = load_data('day13\data.txt')
print(paper)
for cmd in fold_commands:
    paper, px, py = fold(paper, cmd, px, py)

print(paper)
