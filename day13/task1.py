import numpy as np

def load_data(filename):
    file = open(filename)
    pairs = []
    state = False
    for row in file:
        if(row == '\n'):
            state = True
            continue
        else:
            if not state:
                row = row[:-1]
                tmp_split = row.split(',')
                pairs.append((int(tmp_split[0]), int(tmp_split[1])))
            else:
                row_split = row.split()
                fold = row_split[2]
                break

    maxx = -1
    maxy = -1
    for pair in pairs:
        if pair[0] > maxx:
            maxx = pair[0]
        if pair[1] > maxy:
            maxy = pair[1]
    paper = np.zeros((maxy + 1, maxx + 1))

    # Mark pairs on paper
    for pair in pairs:
        paper[pair[1]][pair[0]] = 1
    px = maxx + 1
    py = maxy + 1
    return paper, fold, px, py

def fold_x(paper, line, px, py):
    new_paper = np.zeros((py, line))

    # Mark original
    for i in range(py):
        for j in range(line):
            new_paper[i][j] = paper[i][j]

    # Mark folded
    for i in range(py):
        for j in range(line):
            if paper[i][px - j - 1] == 1:
                new_paper[i][j] = 1

    return new_paper, line, py

def fold_y(paper, line, px, py):
    new_paper = np.zeros((line, px))

    # Mark original
    for i in range(line):
        for j in range(px):
            new_paper[i][j] = paper[i][j]

    # Mark folded
    for i in range(line):
        for j in range(px):
            if paper[py - i - 1][j] == 1:
                new_paper[i][j] = 1

    return new_paper, px, line

def fold(paper, fold_command, px, py):
    tmp_cmd = fold_command.split('=')
    if(tmp_cmd[0] == 'x'):
        new_paper, px, py = fold_x(paper, int(tmp_cmd[1]), px, py)
    else:
        new_paper, px, py = fold_y(paper, int(tmp_cmd[1]), px, py)
    
    print(new_paper)
    print(count_dots(new_paper, px, py))

def count_dots(paper, x, y):
    cnt = 0
    for i in range(y):
        for j in range(x):
            if paper[i][j] == 1:
                cnt += 1

    return cnt


paper, fold_command, px, py = load_data('day13\data.txt')
print(paper)
fold(paper, fold_command, px, py)
