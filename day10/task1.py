errors = {
    ')' : 3,
    ']' : 57,
    '}' : 1197,
    '>' : 25137
}


error_sum = 0

file = open('day10\data.txt')
for row in file:
    signs = []
    signs[:0] = row
    signs = signs[:-1]
    
    stack = []
    for sign in signs:
        if sign == '(':
            stack.append(')')
        elif sign == '[':
            stack.append(']')
        elif sign == '{':
            stack.append('}')
        elif sign == '<':
            stack.append('>')
        else:
            if stack.pop() != sign:
                error_sum += errors[sign]
                break

print(error_sum)