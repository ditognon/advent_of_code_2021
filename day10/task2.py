import math

points = {
    ')' : 1,
    ']' : 2,
    '}' : 3,
    '>' : 4
}

products = []

file = open('day10\data.txt')
for row in file:
    signs = []
    signs[:0] = row
    signs = signs[:-1]
    
    stack = []
    corrupted = False
    for sign in signs:
        if sign == '(':
            stack.append(')')
        elif sign == '[':
            stack.append(']')
        elif sign == '{':
            stack.append('}')
        elif sign == '<':
            stack.append('>')
        else:
            if stack.pop() != sign:
                corrupted = True
                break

    if not corrupted:
        product = 0
        for i in range(len(stack)):
            product = product * 5 + points[stack.pop()]
        products.append(product)

# Find median
products.sort()
print(products)
med_index = math.floor(len(products) / 2)
print(products[med_index])