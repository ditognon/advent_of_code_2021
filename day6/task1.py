
file = open('day6\data.txt')

def load_data(file):
    row = file.readline()
    population = row.split(',')
    for i in range(len(population)):
        population[i] = int(population[i])
    return population

population = load_data(file)

def simulate(population, days):
    next_pop = population.copy()
    for d in range(days):
        population = next_pop.copy()
        next_pop.clear()
        for i in population:
            if i == 0:
                next_pop.append(6)
                next_pop.append(8)
            else:
                next_pop.append(i - 1)
    return (next_pop, len(next_pop))

res_pop, size = simulate(population, 80)

print(size)