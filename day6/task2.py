
file = open('day6\data.txt')

def load_data(file):
    row = file.readline()
    population = row.split(',')
    counters = [0] * 9
    for i in range(len(population)):
        population[i] = int(population[i])

    for p in population:
        counters[p] += 1
    
    return counters

counters = load_data(file)

def simulate(counters, days):
    max = 8
    for d in range(days):
        tmp = counters[0]
        for i in range(len(counters)):
            if i != 0:
                counters[i - 1] = counters[i]
        counters[8] = 0
        counters[6] += tmp
        counters[8] += tmp
        
    size = 0
    for i in counters:
        size += i

    return size

size = simulate(counters, 256)

print(size)