file = open('day2\data.txt')
pos = (0, 0) # X, Y
for row in file:
    input = row.split()
    command = input[0]
    value = int(input[1])
    if(command == 'forward'):
        pos = (pos[0] + value, pos[1])
    elif(command == 'up'):
        pos = (pos[0], pos[1] - value)
    elif(command == 'down'):
        pos = (pos[0], pos[1] + value)
    else:
        print("Invalid command!")
    #print(pos)

print(pos)
print("Position product = " + str(pos[0] * pos[1]))