class Node:
    def __init__(self, name):
        self.neighbors = []
        self.name = name

    def add_neighbor(self, neighbor):
        self.neighbors.append(neighbor)

    def __str__(self):
        return self.name

class Path:
    def __init__(self):
        self.expl_small_caves = []
        self.nodes = []
    def add_node(self, node):
        self.nodes.append(node)
    def add_small_cave(self, node):
        self.expl_small_caves.append(node)
    def copy(self):
        np = Path()
        np.nodes = self.nodes.copy()
        np.expl_small_caves = self.expl_small_caves.copy()
        return np
    def __str__(self):
        return str(self.nodes)

# Returns start node
def load_data(filename):
    file = open(filename)

    nodes = {}

    for row in file:
        row = row[:-1] # Remove \n
        split_row = row.split('-')

        if split_row[0] in nodes:
            node1 = nodes[split_row[0]]
        else:
            node1 = Node(split_row[0])
            nodes[node1.name] = node1

        if split_row[1] in nodes:
            node2 = nodes[split_row[1]]
        else:
            node2 = Node(split_row[1])
            nodes[node2.name] = node2

        node1.add_neighbor(node2)
        node2.add_neighbor(node1)

    return nodes['start']

def recursive_search(node, path):
    path.add_node(node)
    if node.name.islower():
        path.add_small_cave(node)
    if node.name == 'end':
        print('Found path: ' + str(path))
        paths.append(path)
    for n in node.neighbors:
        if n not in path.expl_small_caves:
            recursive_search(n, path.copy())

start_node = load_data('day12\data.txt')

# Find paths

explored_small_caves = []
paths =[]
recursive_search(start_node, Path())

for p in paths:
    print()
    for n in p.nodes:
        print(n.name, end=' ')
print(len(paths))




