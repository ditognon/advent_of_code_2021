def load_data(filename):
    file = open(filename)
    line = file.readline()[:-1]
    x_part = line.split()[2][2:-1]
    y_part = line.split()[3][2:]
    
    xl = int(x_part.split('..')[0])
    xh = int(x_part.split('..')[1])
    yl = int(y_part.split('..')[0])
    yh = int(y_part.split('..')[1])

    return (xl, xh, yl, yh)

def in_area(pos, area):
    if pos[0] >= area[0] and pos[0] <= area[1]:
        if pos[1] >= area[2] and pos[1] <= area[3]:
            return True
    return False

def next_step(pos_vel):
    n_pos_x = pos_vel[0] + pos_vel[2]
    n_pos_y = pos_vel[1] + pos_vel[3]

    if pos_vel[2] > 0:
        n_vel_x = pos_vel[2] - 1
    elif pos_vel[2] < 0:
        n_vel_x = pos_vel[2] + 1
    else:
        n_vel_x = 0

    n_vel_y  = pos_vel[3] - 1

    return (n_pos_x, n_pos_y, n_vel_x, n_vel_y)

def past_area(pos, target):
    if pos[0] > target[1] and pos[2] >= 0:
        return True
    
    if pos[0] < target[0] and pos[2] <= 0:
        return True

    if pos[1] < target[2]:
        return True

    return False


# True if probe hits target area
# False if it doesn't
def simulate_movement(start_pos, target):
    pos = start_pos
    #print(pos)
    while(not in_area(pos, target) and not past_area(pos, target)):
        pos = next_step(pos)

    if in_area(pos, target):
        return True
    return False

target_area = load_data('day17/data.txt')
cnt = 0
for y in range(-141, 141):
    for x in range(1, 177):
        t = simulate_movement((0, 0, x, y), target_area)
        if t:
            cnt += 1

print('Different solution: ', cnt)
