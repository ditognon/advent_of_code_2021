def load_data(filename):
    file = open(filename)

    input = file.readline()[:-1]

    file.readline() # Skip empty line
    vars = set()
    rules = {}
    for row in file:
        tmp_split = row.split(' -> ')
        rules[tmp_split[0]] = tmp_split[1][:-1]
        vars.add(tmp_split[1][:-1])

    return (input, rules, vars)

memory = {}
def rec(input, step, cnt):
    if (step == 0):
        return None
    elif (step == 1):
        cnt[rules[input]] += 1
        tmp = {rules[input]:1}
        return tmp
    else:
        if (input, step) in memory:
            #print('using memory')
            tmp_cnt = memory[(input, step)]
            for k in tmp_cnt:
                cnt[k] += tmp_cnt[k]
            return tmp_cnt
        else:
            tmp_mem = {}
            ret = rec(input[0] + rules[input], step - 1, cnt)
            if ret is not None:
                for k in ret:
                    tmp_mem[k] = ret[k]

            cnt[rules[input]] += 1
            if rules[input] in tmp_mem:
                tmp_mem[rules[input]] += 1
            else:
                tmp_mem[rules[input]] = 1
        
            ret = rec(rules[input] + input[1], step - 1, cnt)
            if ret is not None:
                for k in ret:
                    if k in tmp_mem:
                        tmp_mem[k] += ret[k]
                    else:
                        tmp_mem[k] = ret[k]
            memory[(input, step)] = tmp_mem
            return tmp_mem

def solve_problem(input, step):
    cnt = {}
    for var in vars:
        cnt[var] = 0
    for i in range(1, len(input)):
        cnt[input[i - 1]] += 1
        rec(input[i - 1] + input[i], step, cnt)
        if i == len(input) - 1:
            cnt[input[i]] += 1

    return cnt

input, rules, vars = load_data('day14\data.txt')

cnt = solve_problem(input, 40)

max = -1
min = -1

for key in cnt:
    if cnt[key] > max:
        max = cnt[key]

    if cnt[key] < min or min == -1:
        min = cnt[key]

print(max - min)

