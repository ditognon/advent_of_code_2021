def load_data(filename):
    file = open(filename)

    input = file.readline()[:-1]

    file.readline() # Skip empty line
    vars = set()
    rules = {}
    for row in file:
        tmp_split = row.split(' -> ')
        rules[tmp_split[0]] = tmp_split[1][:-1]
        vars.add(tmp_split[1][:-1])

    return (input, rules, vars)

input, rules, vars = load_data('day14\data_example.txt')


input_list = []
input_list[:0] = input
for step in range(5):
    i = 1
    while i < len(input_list):
        comb = input_list[i - 1] + input_list[i]
        if comb in rules:
            input_list.insert(i, rules[comb])
            i += 2
        else:
            i += 1

cnt = {}
for var in vars:
    cnt[var] = 0

for i in input_list:
    cnt[i] += 1

max = -1
min = -1

for key in cnt:
    if cnt[key] > max:
        max = cnt[key]

    if cnt[key] < min or min == -1:
        min = cnt[key]

print(max - min)

