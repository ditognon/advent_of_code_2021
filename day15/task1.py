from queue import PriorityQueue

class State:
    def __init__(self, i, j, cost, real_cost):
        self.i = i
        self.j = j
        self.cost = cost
        self.real_cost = real_cost
        self.removed = False

    def __str__(self):
        return '(' + str(self.i) + ', ' + str(self.j) + ', ' + 'cost=' + str(self.cost) + ')'

    def __eq__(self, other):
        return self.i == other.i and self.j == other.j

    def equal(self, other):
        return self.i == other.i and self.j == other.j and self.cost == other.cost

    def __lt__(self, other):
        return self.cost < other.cost

    def __gt__(self, other):
        return self.cost > other.cost
    
    def __le__(self, other):
        return self.cost <= other.cost

    def __ge__(self, other):
        return self.cost >= other.cost

def load_data(filename):
    file = open(filename)
    table = []
    for row in file:
        row_tmp = []
        row_tmp[:0] = row[:-1]
        row_tmp = [int(k) for k in row_tmp]
        table.append(row_tmp)

    return table

def manhattan_distance(a):
    return end_i - a[0] + end_j - a[1]

def is_in(element, list):
    for l in list:
        if l.__eq__(element):
            return l
    return None

# A star-search
# State is a tuple(x, y)
def a_star_least_risk(start, table, goal):
    open = PriorityQueue()
    open_list = []
    start_state = State(start[0], start[1], 0, 0)
    open.put(start_state)
    open_list.append(start_state)
    closed = []

    while not open.empty():
        n = open.get()
        if n.removed:
            continue
        #print(n)
        open_list.remove(n)

        if n.i == goal[0] and n.j == goal[1]:
            return n

        closed.append(n)

        succ = []
        if n.i > 0:
            succ.append(State(n.i - 1, n.j, n.real_cost + table[n.i - 1][n.j] + manhattan_distance((n.i - 1, n.j)), n.real_cost + table[n.i - 1][n.j]))
        if n.i < end_i:
            succ.append(State(n.i + 1, n.j, n.real_cost + table[n.i + 1][n.j] + manhattan_distance((n.i + 1, n.j)), n.real_cost + table[n.i + 1][n.j]))
        if n.j > 0:
            succ.append(State(n.i, n.j - 1, n.real_cost + table[n.i][n.j - 1] + manhattan_distance((n.i, n.j - 1)), n.real_cost + table[n.i][n.j - 1]))
        if n.j < end_j:
            succ.append(State(n.i, n.j + 1, n.real_cost + table[n.i][n.j + 1] + manhattan_distance((n.i, n.j + 1)), n.real_cost + table[n.i][n.j + 1]))

        for s in succ:
            el = is_in(s, closed)
            if el is not None:
                if el.cost <= s.cost:
                    continue
                else:
                    closed.remove(el)
                
            el = is_in(s, open_list)
            if el is not None:
                if el.cost <= s.cost:
                    continue
                else:
                    open_list.remove(el)
                    el.removed = True
                    #print('el ', el)
                    #print('s ', s)
            open.put(s)
            open_list.append(s)

    return None
                

table = load_data('day15\data.txt')
table[0][0] = 0
end_i = len(table) - 1
end_j = len(table[0]) - 1

goal = a_star_least_risk((0, 0), table, (end_i, end_j))
print(goal.real_cost)