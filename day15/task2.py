import numpy as np
import heapq

def load_data(filename):
    file = open(filename)
    table = []
    for row in file:
        row_tmp = []
        row_tmp[:0] = row[:-1]
        row_tmp = [int(k) for k in row_tmp]
        table.append(row_tmp)

    return table

def manhattan_distance(i, j, ei, ej):
    return ei - i + ej - j

# A star-search
# State is a tuple(cost, x, y)
def a_star_least_risk(start, table, goal):
    e_i = len(table) - 1
    e_j = len(table[0]) - 1
    open_array = np.zeros((e_i + 1, e_j + 1))
    closed = np.zeros((e_i + 1, e_j + 1))
    for i in range(e_i + 1):
        for j in range(e_j + 1):
            open_array[i][j] = -1
            closed[i][j] = -1
    open = []
    start_state = (manhattan_distance(start[0], start[1], e_i, e_j), start[0], start[1])
    heapq.heappush(open, start_state)
    open_array[start_state[1]][start_state[2]] = start_state[0]
    succ = []
    while True:
        n = heapq.heappop(open)
        open_array[n[1]][n[2]] = -1

        if n[1] == goal[0] and n[2] == goal[1]:
            return n

        closed[n[1]][n[2]] = n[0]

        succ.clear()
        if n[1] > 0:
            succ.append((n[0] - manhattan_distance(n[1], n[2], e_i, e_j) + manhattan_distance(n[1] - 1, n[2], e_i, e_j) + table[n[1] - 1][n[2]], n[1] - 1, n[2]))
        if n[1] < e_i:
            succ.append((n[0] - manhattan_distance(n[1], n[2], e_i, e_j) + manhattan_distance(n[1] + 1, n[2], e_i, e_j) + table[n[1] + 1][n[2]], n[1] + 1, n[2]))
        if n[2] > 0:
            succ.append((n[0] - manhattan_distance(n[1], n[2], e_i, e_j) + manhattan_distance(n[1], n[2] - 1, e_i, e_j) + table[n[1]][n[2] - 1], n[1], n[2] - 1))
        if n[2] < e_j:
            succ.append((n[0] - manhattan_distance(n[1], n[2], e_i, e_j) + manhattan_distance(n[1], n[2] + 1, e_i, e_j) + table[n[1]][n[2] + 1], n[1], n[2] + 1))

        for s in succ:
            el = closed[s[1]][s[2]]
            if el != -1:
                if el <= s[0]:
                    continue
                else:
                    closed[s[1]][s[2]] = -1
                
            el = open_array[s[1]][s[2]]
            if el != -1:
                if el <= s[0]:
                    continue
                else:
                    open.remove((el, s[1], s[2]))

            heapq.heappush(open, s)
            open_array[s[1]][s[2]] = s[0]
                
def enlarge_table(table):
    end_i = len(table)
    end_j = len(table[0])
    tablex5 = np.zeros((5 * end_i, 5 * end_j))

    for i_t in range(5):
        for j_t in range(5):
            for i in range(end_i):
                for j in range(end_j):
                    if i_t > 0:
                        if tablex5[(i_t - 1) * end_i + i][j_t * end_j + j] + 1 == 10:
                            tablex5[i_t * end_i + i][j_t * end_j + j] = 1
                        else:
                            tablex5[i_t * end_i + i][j_t * end_j + j] = tablex5[(i_t - 1) * end_i + i][j_t * end_j + j] + 1
                    elif j_t > 0:
                        if tablex5[i_t * end_i + i][(j_t - 1) * end_j + j] + 1 == 10:
                            tablex5[i_t * end_i + i][j_t * end_j + j] = 1
                        else:
                            tablex5[i_t * end_i + i][j_t * end_j + j] = tablex5[i_t * end_i + i][(j_t - 1) * end_j + j] + 1
                    else:
                        tablex5[i_t * end_i + i][j_t * end_j + j] = table[i][j]
    return tablex5

table = load_data('day15\data.txt')
tablex5 = enlarge_table(table)

tablex5[0][0] = 0

goal = a_star_least_risk((0, 0), tablex5, (len(tablex5) - 1, len(tablex5[0]) - 1))
print(goal[0])
