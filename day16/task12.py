class Packet:
    def __init__(self, version, type_id, data=None):
        self.packets = []
        self.version = version
        self.type_id = type_id
        self.data = data

def load_data(filename):
    file = open(filename)
    return file.readline()[:-1]

def hex_to_bin(hex_input):
    translate = {
        '0' : '0000',
        '1' : '0001',
        '2' : '0010',
        '3' : '0011',
        '4' : '0100',
        '5' : '0101',
        '6' : '0110',
        '7' : '0111',
        '8' : '1000',
        '9' : '1001',
        'A' : '1010',
        'B' : '1011',
        'C' : '1100',
        'D' : '1101',
        'E' : '1110',
        'F' : '1111',
    }

    bin = ''
    for h in hex_input:
        bin += translate[h]

    return bin

def decode_number(input):
    index = 0
    ibit = input[index]
    bin_num = ''
    while(ibit == '1'):
        bin_num += input[index + 1 : index + 5]
        index += 5
        ibit = input[index]
    bin_num += input[index + 1: index + 5]

    return (int(bin_num, 2), index + 5)

def calculate(type_id, packages):
    res = 0
    if type_id == 0:
        for p in packages:
            res += p.data
    elif type_id == 1:
        res = packages[0].data
        for i in range(1, len(packages)):
            res *= packages[i].data
    elif type_id == 2:
        res = -1
        for p in packages:
            if p.data < res or res == -1:
                res = p.data
    elif type_id == 3:
        res = -1
        for p in packages:
            if p.data > res or res == -1:
                res = p.data
    elif type_id == 5:
        if packages[0].data > packages[1].data:
            return 1
        return 0
    elif type_id == 6:
        if packages[0].data < packages[1].data:
            return 1
        return 0
    elif type_id == 7:
        if packages[0].data == packages[1].data:
            return 1
        return 0
    return res

def extract_packet(wb, n_packets=None):
    packets = []
    start_length = len(wb)

    while len(wb) > 7 and ((n_packets is not None and n_packets > 0) or n_packets is None):
        if n_packets is not None:
            n_packets -= 1
        tmp_version_b = wb[:3]
        tmp_version = int(tmp_version_b, 2)
        tmp_type_id_b = wb[3:6]
        tmp_type_id = int(tmp_type_id_b, 2)
        tmp_packet = Packet(tmp_version, tmp_type_id)
        packets.append(tmp_packet)
        # Packet contains a number
        if tmp_type_id == 4:
            data, length = decode_number(wb[6:])
            tmp_packet.data = data
            wb = wb[length + 6:]
            continue
        # Packet contains packets
        else:
            control_bit = wb[6]
            if control_bit == '0':
                len_packets = int(wb[7:22], 2)
                ret_packets, length = extract_packet(wb[22:22 + len_packets])
                tmp_packet.packets.extend(ret_packets)
                tmp_packet.data = calculate(tmp_packet.type_id, tmp_packet.packets)
                wb = wb[22 + len_packets:]
                continue
            else:
                tmp_n_packets = int(wb[7:18], 2)
                ret_packets, length = extract_packet(wb[18:], tmp_n_packets)
                tmp_packet.packets.extend(ret_packets)
                tmp_packet.data = calculate(tmp_packet.type_id, tmp_packet.packets)
                wb = wb[18 + length:]
                continue
    ret_length = start_length - len(wb)
    return packets, ret_length

hex_input = load_data('day16\data.txt')
bin_input = hex_to_bin(hex_input)
print(bin_input)
packets, ret_length = extract_packet(bin_input)

def rec_count_ver_nums(packets):
    sum = 0
    for packet in packets:
        sum += packet.version
        if len(packet.packets) > 0:
            sum += rec_count_ver_nums(packet.packets)

    return sum

print(packets[0].data)
#print(rec_count_ver_nums(packets))
