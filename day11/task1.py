import numpy as np

def load_data(filename):
    file = open(filename)
    octopuses = []
    for row in file:
        ocs = []
        ocs[:0] = row
        ocs = ocs[:-1]
        ocs = [int(i) for i in ocs]
        octopuses.append(ocs)
    return octopuses

def all_flashed(matrix):
    for i in range(10):
        for j in range(10):
            if matrix[i][j] == 0:
                return False
    return True

octopuses = load_data('day11\data.txt')

shine = 0
step = 0
shined_in_day = np.zeros((10, 10))
while(not all_flashed(shined_in_day)): # For each day
    shined_in_day = np.zeros((10, 10)) # Reset shine checker matrix
    # First - increase energy for each octopus by one
    for i in range(10):
        for j in range(10):
            octopuses[i][j] += 1
    
    old_shine = -1
    while(old_shine != shine):
        old_shine = shine
        for i in range(10):
            for j in range(10):
                if octopuses[i][j] > 9:
                    if shined_in_day[i][j] == 0:
                        shine += 1
                        shined_in_day[i][j] = 1
                        # Add 1 to neighbors
                        if i > 0:
                            if j > 0:
                                octopuses[i - 1][j - 1] += 1
                            if j < len(octopuses[0]) - 1:
                                octopuses[i - 1][j + 1] += 1
                            octopuses[i - 1][j] += 1
                        
                        if i < len(octopuses) - 1:
                            if j > 0:
                                octopuses[i + 1][j - 1] += 1
                            if j < len(octopuses[0]) - 1:
                                octopuses[i + 1][j + 1] += 1
                            octopuses[i + 1][j] += 1

                        if j > 0:
                            octopuses[i][j - 1] += 1
                        
                        if j < len(octopuses[0]) - 1:
                            octopuses[i][j + 1] += 1

    # Reset those which shined to zero
    for i in range(10):
        for j in range(10):
            if octopuses[i][j] > 9:
                octopuses[i][j] = 0

    step += 1

print(step)
