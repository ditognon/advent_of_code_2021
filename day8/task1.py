file = open('day8\data.txt')

def length(s):
    l1 = []
    l1[:0] = s

    return len(l1)

one_counter = 0
four_counter = 0
seven_counter = 0
eight_counter = 0

for row in file:
    tmp = []
    i = 0
    for s in row.split():
        if i > 10: # Count specific digits
            if length(s) == 2:
                one_counter += 1
            elif length(s) == 4:
                four_counter += 1
            elif length(s) == 3:
                seven_counter += 1
            elif length(s) == 7:
                eight_counter += 1
        i += 1

print("RES: ", one_counter + four_counter + seven_counter + eight_counter)