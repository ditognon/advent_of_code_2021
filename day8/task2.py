from typing import SupportsIndex


file = open('day8\data.txt')

nums = {}
nums[0] = ['a', 'c', 'f', 'g', 'e', 'b']
nums[1] = ['c', 'f']
nums[2] = ['a', 'c', 'd', 'e', 'g']
nums[3] = ['a', 'c', 'd', 'f', 'g']
nums[4] = ['b', 'd', 'c', 'f']
nums[5] = ['a', 'b', 'd', 'f', 'g']
nums[6] = ['a', 'b', 'd', 'f', 'g', 'e']
nums[7] = ['a', 'c', 'f']
nums[8] = ['a', 'b', 'c', 'd', 'e', 'f', 'g']
nums[9] = ['a', 'c', 'd', 'b', 'f', 'g']

def convert(string):
    list1=[]
    list1[:0]=string
    return list1

def contains(elements, cont):
    res = cont.copy()
    for e in elements:
        for con in cont:
            c = False
            for ec in con:
                if e == ec:
                    c = True
                    break
            if c == False:
                res.remove(con)

    return res

def find_five(four, one, a, rest):
    five = rest.copy()
    four_w_one = not_in_second(four, one)
    four_w_one.append(a)
    five = contains(four_w_one, rest)

    rest_1 = contains([one[0]], five)
    if(len(rest_1) == 1):
        c = one[0]
        f = one[1]
        six_and_three = contains(one[1], five)
    else:
        c = one[1]
        f = one[0]
        six_and_three = rest_1
    
    four_w_one.append(f)
    
    for k in five:
        if len(not_in_second(k, four_w_one)) == 1:
            five = k
            break

    g = not_in_second(five, four_w_one)[0]

    return (c, f, g, six_and_three)

def not_in_second(first, second):
    diff = []
    for f in first:
        not_in = True
        for s in second:
            if f == s:
                not_in = False
        if not_in:
            diff.append(f)

    return diff

def find_b_d(rest, mapping):
    a_c_f_g = []
    a_c_f_g.append(mapping['a'])
    a_c_f_g.append(mapping['c'])
    a_c_f_g.append(mapping['f'])
    a_c_f_g.append(mapping['g'])

    nine_three = contains(a_c_f_g, rest)
    for nt in nine_three:
        if len(nt) == 5:
            three = nt
            break

    d = not_in_second(three, a_c_f_g)[0]
    a_c_f_g.append(mapping['e'])
    a_c_f_g.append(d)
    b = not_in_second(eight, a_c_f_g)[0]

    return (b, d)

def find_sector_mappings(one, four, seven, eight, rest):
    mapping = {}
    # Find sector a
    mapping['a'] = not_in_second(seven, one)[0]

    # Find sectors g,f,c
    c, f, g, six_and_three = find_five(four, one, mapping['a'], rest)
    mapping['c'] = c
    mapping['f'] = f
    mapping['g'] = g

    four_w_one = not_in_second(four, one)

    five_and_one = []
    five_and_one.append(mapping['c'])
    five_and_one.append(mapping['f'])
    five_and_one.append(mapping['g'])
    five_and_one.append(mapping['a'])
    five_and_one.extend(four_w_one)

    mapping['e'] = not_in_second(eight, five_and_one)[0]

    b, d = find_b_d(rest, mapping)
    mapping['b'] = b
    mapping['d'] = d

    return mapping

def find_num(real_signals, nums):
    for i in range(10):
        if len(nums[i]) == len(real_signals):
            # Does real_signals contain all necessary signals
            for j,o in enumerate(nums[i]):
                cont = False
                for k in real_signals:
                    if o == k:
                        cont = True
                        break
                if not cont:
                    break
                else:
                    if j == (len(nums[i]) - 1):
                        return i

def build_inverted_mappings(mappings):
    inv_mapppings = {}
    for key in mappings:
        inv_mapppings[mappings[key]] = key

    return inv_mapppings

def decipher(signals, mappings):
    real_signals = []
    for s in signals:
        real_signals.append(mappings[s])

    return find_num(real_signals, nums)

total_sum = 0

for row in file:
    i = 0
    res = ''
    unknown = []
    for s in row.split():
        if i <= 9:
            if(len(s) == 2):
                one = convert(s)
            elif(len(s) == 4):
                four = convert(s)
            elif(len(s) == 3):
                seven = convert(s)
            elif(len(s) == 7):
                eight = convert(s)
            else:
                unknown.append(convert(s))
            
        if i == 10:
            mappings = find_sector_mappings(one, four, seven, eight, unknown)
            mappings = build_inverted_mappings(mappings)
            # print(mappings)

        if i > 10:
            digit_signals = convert(s)
            digit = decipher(digit_signals, mappings)
            res += str(digit)

        if(i == len(row.split()) - 1):
            # print("RES: ", res)
            total_sum += int(res)
        
        i += 1

print('Total sum: ', total_sum)
print()
