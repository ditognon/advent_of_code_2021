import math

class Node:
    def __init__(self, parent=None, left=None, right=None, value=None):
        self.parent = parent
        self.left = left
        self.right = right
        self.value = value

    # def __eq__(self, other):
    #     if other is None:
    #         return False
    #     if self.value is not None:
    #         return self.value == other.value
    #     else:
    #         return self.left.__eq__(other.left) and self.right.__eq__(other.right)
    
def rec_make_node_from_number(number, parent=None):
    n = Node()
    n.parent = parent
    if isinstance(number, list):
        n.left = rec_make_node_from_number(number[0], n)
        n.right = rec_make_node_from_number(number[1], n)
    else:
        n.value = number

    return n

def recursive_load_number(line):
    try:
        num = int(line)
        return num
    except:
        pass

    current_number = []
    par_cnt = 0
    for i,l in enumerate(line):
        if l == '[':
            par_cnt += 1
        elif l == ']':
            par_cnt -= 1
        elif l == ',' and par_cnt == 1:
            end_left = i
            start_right = i + 1
            break
        else:
            continue
    
    current_number.append(recursive_load_number(line[1:end_left]))
    current_number.append(recursive_load_number(line[start_right:-1]))

    return current_number

def load_data(filename):
    file = open(filename)

    numbers = []
    for row in file:
        number = rec_make_node_from_number(recursive_load_number(row[:-1]))

        numbers.append(number)

    return numbers

def add_to_first_right(element):
    add_value = element.right.value
    work = element
    while(work.parent is not None and work.parent.right == work):
        work = work.parent

    if work.parent is None:
        return
    else:
        work = work.parent
        if work.right.value is None:
            work = work.right

            while work.left.value is None:
                work = work.left

            work.left.value += add_value
        else:
            work.right.value += add_value
    
def add_to_first_left(element):
    add_value = element.left.value
    work = element
    while(work.parent is not None and work.parent.left == work):
        work = work.parent

    if work.parent is None:
        return
    else:
        work = work.parent
        if work.left.value is None:
            work = work.left

            while work.right.value is None:
                work = work.right

            work.right.value += add_value
        else:
            work.left.value += add_value

def print_node(node):
    if node.value is not None:
        print(node.value, end='')
    else:
        print('[', end='')
        print_node(node.left)
        print(',', end='')
        print_node(node.right)
        print(']', end='')
    

def rec_explode(num, level):
    if num.value is not None:
        return False
    else:
        if level >= 4:
            add_to_first_left(num)
            add_to_first_right(num)
            num.value = 0
            return True

        exploded = rec_explode(num.left, level + 1)
        if exploded:
            return True
        exploded = rec_explode(num.right, level + 1)
        return exploded

def rec_split(num):
    if num.value is not None:
        if num.value >= 10:
            left_node = Node(value=math.floor(num.value / 2))
            right_node = Node(value=math.ceil(num.value / 2))
            new_node = Node(parent=num.parent, left=left_node, right=right_node, value=None)
            left_node.parent = new_node
            right_node.parent = new_node

            if num.parent is not None:
                if num.parent.left == num:
                    num.parent.left = new_node
                else:
                    num.parent.right = new_node
            return True
    else:
        split = rec_split(num.left)
        if split:
            return True
        split = rec_split(num.right)
        return split

def reduce_number(num):
    reduced = True
    while(reduced):
        # print('Reduction -------------')
        # print_node(num)
        # print()
        reduced = False
        exploded = rec_explode(num, 0)
        if exploded:
            reduced = True
            continue

        split = rec_split(num)
        if split:
            reduced = True
            continue

def add_numbers(num1, num2):
    result = Node(left=num1, right=num2)
    num1.parent = result
    num2.parent = result
    reduce_number(result)

    return result

def rec_calculate_magnitude(solution):
    if solution.value is not None:
        return solution.value
    else:
        return rec_calculate_magnitude(solution.left) * 3 + rec_calculate_magnitude(solution.right) * 2

numbers = load_data('day18/data.txt')

res = numbers[0]
for i in range(1, len(numbers)):
    res = add_numbers(res, numbers[i])

print_node(res)
print()
print('Result magnitude: ', rec_calculate_magnitude(res))

# FOR DEBUGGING
# rec_make_node_from_number(recursive_load_number(row[:-1]))

# node = rec_make_node_from_number(recursive_load_number('[[[[5,0],[7,4]],[5,5]],[6,6]]'))
# print(rec_calculate_magnitude(node))