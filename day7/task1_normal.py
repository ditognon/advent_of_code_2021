import math

file = open('day7\data.txt')

def load_data(file):
    tmp = file.readline().split(',')
    crab_positions = []
    for i in tmp:
        crab_positions.append(int(i))

    return crab_positions

crab_positions = load_data(file)

def error_func(solution, positions):
    sum = 0
    for p in positions:
        num = abs(solution - p)
        sum += (num * (num + 1)) / 2

    return sum

def minimize(positions):
    min_sol = min(positions)
    max_sol = max(positions)

    best = min_sol
    best_error = error_func(min_sol, positions)
    for i in range(min_sol, max_sol + 1):
        tmp_err = error_func(i, positions)
        if(tmp_err < best_error):
            best = i
            best_error = tmp_err

    return best, best_error

sol, err = minimize(crab_positions)
print("Solution: ", sol)
print("Fuel: ", err)
