import math

file = open('day7\data_example.txt')

def load_data(file):
    tmp = file.readline().split(',')
    crab_positions = []
    for i in tmp:
        crab_positions.append(int(i))

    return crab_positions

crab_positions = load_data(file)

def error_func(solution, positions):
    sum = 0
    for p in positions:
        sum += abs(solution - p)

    return sum

def derivation(x, positions):
    sum = 0
    for p in positions:
        sum += (x - p)
    
    return 2 * sum

def find_best_lambda(der, solution, positions):
    lb = 1

    der_lb = derivation(solution + lb * der, positions) * der
    while(der_lb < 0):
        lb = lb * 2
        der_lb = derivation(solution + lb * der,  positions) * der

    min_lb = 0
    max_lb = lb

    # Bisection
    k = 0
    while(k < 1000):
        lb = (max_lb + min_lb) / 2
        der_lb = derivation(round(solution - lb * der), positions) * (-der)
        if der_lb < 0.00005 or der_lb > -0.00005: # Probably needs tolerance
            return lb
        if der_lb > 0:
            max_lb = lb
        else:
            min_lb = lb

        k += 1

    return lb

def gr_descent(positions):
    solution = min(positions)
    der = derivation(solution, positions)
    while(der < 0):
        #lb = find_best_lambda(der, solution, positions)
        solution = solution - 0.005 * der

        der = derivation(solution, positions)

    return round(solution)

print(derivation(2, crab_positions))
print(derivation(4, crab_positions))
#sol = gr_descent(crab_positions)
print("Solution: ", sol)
print("Fuel: ", error_func(sol, crab_positions))



