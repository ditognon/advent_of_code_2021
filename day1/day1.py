def task1():
    file = open('day1\data.txt')
    counter = 0
    last = -1
    for row in file:
        if(last != -1):
            if(int(row) > last):
                counter += 1

        last = int(row)

    return counter

def list_sum(list):
    sum = 0
    for a in list:
        sum += int(a)
    return sum

def task2():
    WINDOW_SIZE = 3
    file = open('day1\data.txt')
    counter = 0
    elements = []

    for i, row in enumerate(file):
        elements.append(int(row))
        if(i >= WINDOW_SIZE):
            #print("SLIDE WINDOW 1: " + str(elements[i - WINDOW_SIZE: i]) + " SUM: " + str(list_sum(elements[i - WINDOW_SIZE: i])))
            #print("SLIDE WINDOW 2: " + str(elements[i - WINDOW_SIZE + 1: i  + 1]) + " SUM: " + str(list_sum(elements[i - WINDOW_SIZE + 1: i + 1])))
            #print()
            if(list_sum(elements[i - WINDOW_SIZE: i]) < list_sum(elements[i - WINDOW_SIZE + 1: i + 1])):
                counter += 1
    return counter

print("Task 1 solution: " + str(task1()))
print("Task 2 solution: " + str(task2()))