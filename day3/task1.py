file = open('day3\data.txt')
n_rows = 0
for i, row in enumerate(file):
    if i == 0:
        counter_array = [0] * (len(row) - 1) # Initialize counter_array
        
    for l in range(len(row) - 1):
        counter_array[l] += int(row[l])
    
    n_rows += 1

# Initialize results
gamma_rate = ''
epsilon_rate_str = ''

for i, e in enumerate(counter_array):
    if (e / n_rows) > 0.5: # If 1 is more common on this position
        gamma_rate += '1'
        epsilon_rate_str += '0'
    else: # If 0 is more common on this position
        gamma_rate += '0'
        epsilon_rate_str += '1'


gama_rate = int(gamma_rate, 2) # Conversion to decimal
epsilon_rate = int(epsilon_rate_str, 2) # Conversion to decimal

print("gama_rate_str * epsilon_rate_str = " + str(gama_rate * epsilon_rate))