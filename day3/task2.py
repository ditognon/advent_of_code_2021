file = open('day3\data.txt')
n_rows = 0
inputs = []

oxy_solution = []
co2_solution = []

# OXYGEN Part

for row in file:
    if row[len(row) - 1] == '\n':
        inputs.append(row[:-1])
    else:
        inputs.append(row)

print(inputs)

inputs2 = inputs.copy()

for i in range(len(row)): # For each number
    cnt0 = 0
    cnt1 = 0
    if i != 0:
        inputs = oxy_solution.copy()
    oxy_solution.clear()
    # Count how many ones and how many zeros
    for n in inputs:
        if int(n[i]) == 1:
            cnt1 += 1
        else:
            cnt0 += 1
    
    for n in inputs:
        if cnt1 >= cnt0:
            if int(n[i]) == 1:
                oxy_solution.append(n)
        else:
            if int(n[i]) == 0:
                oxy_solution.append(n)
    
    if(len(oxy_solution) == 1):
        break

print(oxy_solution)
oxy_solution = oxy_solution[0]
oxy_solution_real = ''
for i in range(len(oxy_solution)):
    if oxy_solution[i] == '1':
        oxy_solution_real += '1'
    else:
        oxy_solution_real += '0'

oxy_solution = int(oxy_solution_real, 2)
print(oxy_solution)



# CO2 part

for i in range(len(row)): # For each number
    cnt0 = 0
    cnt1 = 0
    if i != 0:
        inputs2 = co2_solution.copy()
    co2_solution.clear()
    # Count how many ones and how many zeros
    for n in inputs2:
        if int(n[i]) == 1:
            cnt1 += 1
        else:
            cnt0 += 1
    
    for n in inputs2:
        if cnt1 >= cnt0:
            if int(n[i]) == 0:
                co2_solution.append(n)
        else:
            if int(n[i]) == 1:
                co2_solution.append(n)
    
    if(len(co2_solution) == 1):
        break

print(co2_solution)
co2_solution = co2_solution[0]
co2_solution_real = ''
for i in range(len(co2_solution)):
    if co2_solution[i] == '1':
        co2_solution_real += '1'
    else:
        co2_solution_real += '0'

co2_solution = int(co2_solution_real, 2)
print(co2_solution)

print('co2 * oxygen = ' + str(co2_solution * oxy_solution))

