
file = open('day4\data.txt')
dim = 5

def load_data(file):
    tables = []

    numbers_string = file.readline().split(',')
    numbers_string[len(numbers_string) - 1] = numbers_string[len(numbers_string) - 1][:-1] # Remove /n
    numbers = [int(num) for num in numbers_string]

    file.readline() # Skip empty line

    table = []
    for row in file:
        if(row == '\n'):
            tables.append(table)
            table = []
        else:
            table_row = row.split()
            table_row = [int(num) for num in table_row]
            table.append(table_row)
    tables.append(table)

    return numbers, tables

numbers, tables = load_data(file)

def calculate_score(table, num):
    sum = 0
    for i in range(dim):
        for j in range(dim):
            if table[i][j] != 'x':
                sum += table[i][j]

    return sum * num



def play_bingo(numbers, tables):
    uncompleted_tables = tables
    for num in numbers: # Last drawn number
        tables = uncompleted_tables
        uncompleted_tables = tables.copy()
        for table in tables:
            for i in range(dim):
                for j in range(dim):
                    if(table[i][j] == num):
                        table[i][j] = 'x'
                        # Check if row or column is full
                        column_full = True
                        row_full = True
                        for k in range(dim):
                            if table[i][k] != 'x':
                                row_full = False
                            if table[k][j] != 'x':
                                column_full = False

                        if column_full or row_full:
                            if(len(uncompleted_tables) == 1):
                                return (uncompleted_tables[0], num)
                            else:
                                uncompleted_tables.remove(table)
                            


table, num = play_bingo(numbers, tables)
print(calculate_score(table, num))