def load_data(filename):
    heightmap = []

    file = open(filename)
    for row in file:
        row_list = []
        row_list[:0] = row
        row_list = row_list[:-1]
        row_list = [int(i) for i in row_list]
        heightmap.append(row_list)

    return heightmap

filename = 'day9\data.txt'
heightmap = load_data(filename)

risk_sum = 0

for i in range(len(heightmap)):
    for j in range(len(heightmap[0])):
        smaller_than_neighbors = True
        if i > 0 and heightmap[i][j] >= heightmap[i - 1][j]:
            smaller_than_neighbors = False
        if i < (len(heightmap) - 1) and heightmap[i][j] >= heightmap[i + 1][j]:
            smaller_than_neighbors = False
        if j > 0 and heightmap[i][j] >= heightmap[i][j - 1]:
            smaller_than_neighbors = False
        if j < (len(heightmap[0]) - 1) and heightmap[i][j] >= heightmap[i][j + 1]:
            smaller_than_neighbors = False
        if smaller_than_neighbors:
            risk_sum += (heightmap[i][j] + 1)

print("Risk sum: ", risk_sum)