def load_data(filename):
    heightmap = []

    file = open(filename)
    for row in file:
        row_list = []
        row_list[:0] = row
        row_list = row_list[:-1]
        row_list = [int(i) for i in row_list]
        heightmap.append(row_list)

    return heightmap
tested = set()
def rec_basin(heightmap, i, j, pos):
    sum = 0
    tested.add((i, j))
    if heightmap[i][j] != 9:
        #print((i, j))
        sum = 1
        if i > 0 and pos != 'down' and ((i-1),j) not in tested:
            sum += rec_basin(heightmap, i - 1, j, 'up')
        if i < len(heightmap) - 1 and pos != 'up' and ((i+1),j) not in tested:
            sum += rec_basin(heightmap, i + 1, j, 'down')
        if j > 0 and pos != 'right' and (i,(j-1)) not in tested:
            sum += rec_basin(heightmap, i, j - 1, 'left')
        if j < len(heightmap[0]) - 1 and pos != 'left' and (i,(j+1)) not in tested:
            sum += rec_basin(heightmap, i, j + 1, 'right')
    return sum

def find_basin_size(heightmap, i, j):
    return rec_basin(heightmap, i, j, 'start')

filename = 'day9\data.txt'
heightmap = load_data(filename)

risk_sum = 0

largest_basins = []
smallest_basin_size = 0

for i in range(len(heightmap)):
    for j in range(len(heightmap[0])):
        smaller_than_neighbors = True
        if i > 0 and heightmap[i][j] >= heightmap[i - 1][j]:
            smaller_than_neighbors = False
        if i < (len(heightmap) - 1) and heightmap[i][j] >= heightmap[i + 1][j]:
            smaller_than_neighbors = False
        if j > 0 and heightmap[i][j] >= heightmap[i][j - 1]:
            smaller_than_neighbors = False
        if j < (len(heightmap[0]) - 1) and heightmap[i][j] >= heightmap[i][j + 1]:
            smaller_than_neighbors = False
        if smaller_than_neighbors:
            tmp_basin_size = find_basin_size(heightmap, i, j)
            if tmp_basin_size > smallest_basin_size:
                if len(largest_basins) == 3:
                    largest_basins.remove(smallest_basin_size)
                    largest_basins.append(tmp_basin_size)
                    largest_basins.sort()
                    smallest_basin_size = largest_basins[0]
                else:
                    largest_basins.append(tmp_basin_size)
                    largest_basins.sort()
                    smallest_basin_size = largest_basins[0]


product = 1
for l in largest_basins:
    product *= l

print("Result: ", product)
