file = open('day5\data.txt')

ix1 = 0
ix2 = 2
iy1 = 1
iy2 = 3

def load_data(file):
    lines = []
    for row in file:
        temp = row.split(' -> ')
        if temp[1][len(temp[1]) - 1] == '\n':
            temp[1] = temp[1][:-1]
        x1 = int(temp[0].split(',')[0])
        y1 = int(temp[0].split(',')[1])
        x2 = int(temp[1].split(',')[0])
        y2 = int(temp[1].split(',')[1])
        lines.append((x1, y1, x2, y2))

    return lines

def filter_lines(lines):
    filtered_lines = []

    for line in lines:
        if line[ix1] == line[ix2] or line[iy1] == line[iy2]:
            filtered_lines.append(line)
    return filtered_lines

def find_min_max(lines):
    minx = -1
    maxx = -1
    miny = -1
    maxy = -1
    for line in lines:
        # x1
        if line[ix1] > maxx or maxx == -1:
            maxx = line[ix1]
        if line[ix1] < minx or minx == -1:
            minx = line[ix1]

        #x2
        if line[ix2] > maxx or maxx == -1:
            maxx = line[ix2]
        if line[ix2] < minx or minx == -1:
            minx = line[ix2]
        
        #y1
        if line[iy1] > maxy or maxy == -1:
            maxy = line[iy1]
        if line[iy1] < miny or miny == -1:
            miny = line[iy1]

        #y2
        if line[iy2] > maxy or maxy == -1:
            maxy = line[iy2]
        if line[iy2] < miny or miny == -1:
            miny = line[iy2]

    return (minx, maxx, miny, maxy)

def create_graph(lines):
    #minx, maxx, miny, maxy = find_min_max(lines)
    graph = []
    for i in range(1001):
        graph.append([0] * 1001)

    return graph

# Doesn't work with diagonal lines
def paint_graph(graph, lines):
    for line in lines:
        # horizontal
        if(line[iy1] == line[iy2]):
            if line[ix1] > line[ix2]:
                for i in range(line[ix2], line[ix1] + 1):
                    graph[line[iy1]][i] += 1
            else:
                for i in range(line[ix1], line[ix2] + 1):
                    graph[line[iy1]][i] += 1
        # vertical
        else:
            if line[iy1] > line[iy2]:
                for i in range(line[iy2], line[iy1] + 1):
                    graph[i][line[ix1]] += 1
            else:
                for i in range(line[iy1], line[iy2] + 1):
                    graph[i][line[ix1]] += 1
    return graph

def count_2s(graph):
    counter = 0
    for i in range(len(graph)):
        for j in range(len(graph[0])):
            if graph[i][j] >= 2:
                counter += 1

    return counter

lines = load_data(file)
lines = filter_lines(lines)
graph = create_graph(lines)

graph = paint_graph(graph, lines)
result = count_2s(graph)
print(result)
